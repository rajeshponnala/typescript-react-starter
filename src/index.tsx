import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { register } from './registerServiceWorker'

ReactDOM.render(<h1>Hello, World!</h1>, document.getElementById(
  'root',
) as HTMLElement)

register()
