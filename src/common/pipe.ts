type UnaryFunction<T, R> = { (source: T): R }

export function pipe<T, A>(obj: T, op1: UnaryFunction<T, A>): A
export function pipe<T, A, B>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
): B
export function pipe<T, A, B, C>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
): C
export function pipe<T, A, B, C, D>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
): D
export function pipe<T, A, B, C, D, E>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
  op5: UnaryFunction<D, E>,
): E
export function pipe<T, A, B, C, D, E, F>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
  op5: UnaryFunction<D, E>,
  op6: UnaryFunction<E, F>,
): F
export function pipe<T, A, B, C, D, E, F, G>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
  op5: UnaryFunction<D, E>,
  op6: UnaryFunction<E, F>,
  op7: UnaryFunction<F, G>,
): G
export function pipe<T, A, B, C, D, E, F, G, H>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
  op5: UnaryFunction<D, E>,
  op6: UnaryFunction<E, F>,
  op7: UnaryFunction<F, G>,
  op8: UnaryFunction<G, H>,
): H
export function pipe<T, A, B, C, D, E, F, G, H, I>(
  obj: T,
  op1: UnaryFunction<T, A>,
  op2: UnaryFunction<A, B>,
  op3: UnaryFunction<B, C>,
  op4: UnaryFunction<C, D>,
  op5: UnaryFunction<D, E>,
  op6: UnaryFunction<E, F>,
  op7: UnaryFunction<F, G>,
  op8: UnaryFunction<G, H>,
  op9: UnaryFunction<H, I>,
): I

// tslint:disable

export function pipe(
  obj: any,
  first: (obj: any) => any,
  ...rest: Array<(obj: any) => any>
): any {
  let result: any = first(obj)
  for (const fn of rest) {
    result = fn(result)
  }
  return result
}
