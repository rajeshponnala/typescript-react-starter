export interface PlainObject {
  readonly [prop: string]: any
}

export type Diff<T extends string, U extends string> = ({ [P in T]: P } &
  { [P in U]: never } & { readonly [x: string]: never })[T]

export type Omit<T, K extends keyof T> = Pick<T, Diff<keyof T, K>>

export type Overwrite<T, U> = Omit<T, Diff<keyof T, Diff<keyof T, keyof U>>> & U

export type Booleanify<T> = { readonly [P in keyof T]: boolean }

// What about optional?
export type Stringify<T> = { readonly [P in keyof T]: string }
