import { PlainObject, Omit } from './types'

export const merge = <T1 extends PlainObject, T2 extends PlainObject>(
  obj1: T1,
  obj2: T2,
): T1 & T2 => {
  const result: any = {}
  Object.assign(result, obj1)
  Object.assign(result, obj2)
  return result
}

export const mergeDefaults = <T extends PlainObject>(
  defaults: T,
  obj: Partial<T>,
): T => merge(obj, defaults) as T

export const get = <T extends PlainObject, K extends keyof T>(
  obj: T,
  k: K,
): T[K] => obj[k]

// tslint:disable

export const range = (start: number, stop?: number): ReadonlyArray<number> => {
  if (stop === undefined) {
    stop = start
    start = 0
  }
  const result = []
  for (let i = start; i < stop; i += 1) {
    result.push(i)
  }
  return result
}

export const pluck = <T extends PlainObject, K extends keyof T>(
  arr: ReadonlyArray<T>,
  ...keys: K[]
): ReadonlyArray<Pick<T, K>> => {
  const result: any[] = []
  for (const obj of arr) {
    result.push(pick(obj, ...keys))
  }
  return result
}

export const pick = <T extends PlainObject, K extends keyof T>(
  obj: T,
  ...keys: K[]
): Pick<T, K> => {
  const result: any = {}
  for (const k of keys) {
    result[k] = obj[k]
  }
  return result
}

export const set = <T extends PlainObject, K extends keyof T>(
  obj: T,
  k: K,
  v: T[K],
): T => {
  const result: any = {}
  Object.assign(result, obj)
  result[k] = v
  return result
}

export const update = <T extends PlainObject, K extends keyof T, T2>(
  obj: T,
  k: K,
  f: (x: T[K]) => T2,
): T2 => {
  const result: any = {}
  Object.assign(result, obj)
  result[k] = f(obj[k])
  return result
}

export const remove = <T extends PlainObject, K extends keyof T>(
  obj: T,
  key: K,
): Omit<T, K> => {
  const result: any = {}
  for (const k in obj) {
    if (obj.hasOwnProperty(k) && key !== k) {
      result[k] = obj[k]
    }
  }
  return result
}
